#ifndef __suexec_strlist_H
#define __suexec_strlist_H 1

#include "log.h"

#include <stdlib.h>

static int strlist_append(char ***strp, int *szp, char *s) {
  if (!(*strp))
    *strp = (char **)malloc(sizeof(char *));
  else
    *strp = (char **)realloc(*strp, sizeof(char *) * (*szp + 1));
  if (!(*strp)) {
    log_println("out of memory");
    return -1;
  }
  *strp[(*szp)++] = strdup(s);
  return 0;
}



#endif
