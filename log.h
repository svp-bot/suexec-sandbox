#ifndef __suexec_log_H
#define __suexec_log_H 1

#include <errno.h>
#include <stdio.h>
#include <string.h>

#define LOG_PREFIX "suexec: "
#define log_println(fmt) fprintf(stderr, LOG_PREFIX fmt "\n")
#define log_println_errno(fmt) fprintf(stderr, LOG_PREFIX fmt ": %s\n", strerror(errno))
#define log_printf(fmt, ...) fprintf(stderr, LOG_PREFIX fmt "\n", ##__VA_ARGS__)
#define log_printf_errno(fmt, ...) fprintf(stderr, LOG_PREFIX fmt "%s\n", ##__VA_ARGS__, strerror(errno))

#endif
