#! /bin/sh

if [ -x "`which autoreconf 2>/dev/null`" ] ; then
  exec autoreconf -ivf
fi

aclocal -I m4 && \
autoheader && \
automake --add-missing --foreign && \
autoconf
